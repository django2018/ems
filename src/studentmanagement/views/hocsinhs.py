from django.shortcuts import render
from django.conf import settings
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.http.response import HttpResponseRedirect
from ..forms import * 
from ..models import *
from ..utilities import *
from django.contrib.auth.models import User

def cu_lop(request, id=0):
    if not id is None and int(id) > 0:
        obj = Lop.objects.get(pk=id)
    else:
        obj = Lop()
    if request.method == 'POST':
        data = request.POST.copy()
        form = LopForm(data, instance=obj)
        if form.is_valid():
            lop = form.save()
            c_giangday(lop)
    else:
        form = LopForm(instance=obj)
    params = {
        'form': form,
    }
    return render(request, 'lop.html', params)

def show_lops(request):
    objs = Lop.objects.all()
    paginator = Paginator(objs, settings.PAGINATE_BY)
    currentpage = int(request.GET.get('page', 1))
    num_pages = paginator.num_pages
        
    params = {
        'objs': paginator.get_page(currentpage),
    }
    return render(request, 'lops.html', params)

def delete_lop(request, id=0):
    if not id is None and int(id) > 0:
        obj = Lop.objects.get(pk=id)
        obj.delete()
        return HttpResponseRedirect(reverse('studentmanagement_views_lops'))

def cu_hocsinh(request, id=0):
    if not id is None and int(id) > 0:
        obj = HocSinh.objects.get(pk=id)
    else:
        obj = HocSinh()
    if request.method == 'POST':
        data = request.POST.copy()
        form = HocSinhForm(data, instance=obj)
        if form.is_valid():
            obj = form.save()
            if id is None or int(id) == 0:
                user = User(username=giaovien.maso, password=giaovien.maso)
                user.save()
                c_bangdiem(obj)
    else:
        hocsinh = HocSinh.objects.all().last()
        if hocsinh:
            mahocsinh = "{}{:05}".format(1, hocsinh.id)
        else:
            mahocsinh = "{}{:05}".format(1, 0)
        form = HocSinhForm(initial={'mahocsinh': mahocsinh, }, instance=obj)
    params = {
        'form': form,
    }
    return render(request, 'hocsinh.html', params)

def show_hocsinhs(request):
    objs = HocSinh.objects.all()
    paginator = Paginator(objs, settings.PAGINATE_BY)
    currentpage = int(request.GET.get('page', 1))
    num_pages = paginator.num_pages
        
    params = {
        'objs': paginator.get_page(currentpage),
    }
    return render(request, 'hocsinhs.html', params)

def delete_hocsinh(request, id=0):
    if not id is None and int(id) > 0:
        obj = HocSinh.objects.get(pk=id)
        obj.delete()
        return HttpResponseRedirect(reverse('studentmanagement_views_hocsinhs'))

def cu_monhoc(request, id=0):
    if not id is None and int(id) > 0:
        obj = MonHoc.objects.get(pk=id)
    else:
        obj = MonHoc()
    if request.method == 'POST':
        data = request.POST.copy()
        form = MonHocForm(data, instance=obj)
        if form.is_valid():
            form.save()
    else:
        form = MonHocForm(instance=obj)
    params = {
        'form': form,
    }
    return render(request, 'monhoc.html', params)

def show_monhocs(request):
    objs = MonHoc.objects.all()
    paginator = Paginator(objs, settings.PAGINATE_BY)
    currentpage = int(request.GET.get('page', 1))
    num_pages = paginator.num_pages
        
    params = {
        'objs': paginator.get_page(currentpage),
    }
    return render(request, 'monhocs.html', params)

def delete_monhoc(request, id=0):
    if not id is None and int(id) > 0:
        obj = MonHoc.objects.get(pk=id)
        obj.delete()
        return HttpResponseRedirect(reverse('studentmanagement_views_monhocs'))

def show_danhsachlop(request, malop):
    lop = Lop.objects.get(malop=malop)
    hocsinhs = HocSinh.objects.filter(lop=lop)
    paginator = Paginator(hocsinhs, settings.PAGINATE_BY)
    currentpage = int(request.GET.get('page', 1))
    num_pages = paginator.num_pages
        
    params = {
        'objs': paginator.get_page(currentpage),
        'lop': lop,
    }
    return render(request, 'danhsachlop.html', params)

def show_bangdiem(request, mahocsinh):
    danhsachs = BangDiem.objects.all().select_related(
        'hocsinh__lop', 'monhoc'
    ).order_by('hocsinh__lop__tenlop', 'hocsinh__tenhocsinh')
    paginator = Paginator(danhsachs, settings.PAGINATE_BY)
    currentpage = int(request.GET.get('page', 1))
    num_pages = paginator.num_pages
        
    params = {
        'objs': paginator.get_page(currentpage),
    }
    return render(request, 'danhsachhocsinh.html', params)
    
