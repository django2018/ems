from django.conf import settings
from django.shortcuts import render
from ..forms import *
from ..models import *
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.http.response import HttpResponseRedirect
import os
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User

TEMPLATE_PATH = 'giaovien'

def cu_giaovien(request, id=0):
    if not id is None and int(id) > 0:
        obj = GiaoVien.objects.get(pk=id)
    else:
        obj = GiaoVien()
    if request.method == 'POST':
        data = request.POST.copy()
        form = GiaoVienForm(data, instance=obj)
        if form.is_valid():
            giaovien = form.save()
            if int(id) == 0 or id is None:
                user = User(username=giaovien.maso, password=giaovien.maso)
                user.save()
    else:
        giaovien = GiaoVien.objects.all().last()
        if giaovien:
            maso = "{}{:05}".format(1, giaovien.id)
        else:
            maso = "{}{:05}".format(1, 0)
        form = GiaoVienForm(initial={'maso': maso,}, instance=obj)
    params = {
        'form': form,
    }
    return render(request, os.path.join(TEMPLATE_PATH, 'giaovien.html'), params)

def show_giaoviens(request):
    objs = GiaoVien.objects.all()
    paginator = Paginator(objs, settings.PAGINATE_BY)
    currentpage = int(request.GET.get('page', 1))
    num_pages = paginator.num_pages
        
    params = {
        'objs': paginator.get_page(currentpage),
    }
    return render(request, os.path.join(TEMPLATE_PATH, 'giaoviens.html'), params)

def delete_giaovien(request, id=0):
    if not id is None and int(id) > 0:
        obj = GiaoVien.objects.get(pk=id)
        obj.delete()
        return HttpResponseRedirect(reverse('studentmanagement:giaoviens'))

def giangdays(request):
    objs = GiangDay.objects.all().order_by('lop', 'mon')
    paginator = Paginator(objs, settings.PAGINATE_BY)
    currentpage = int(request.GET.get('page', 1))
    num_pages = paginator.num_pages
    
    params = {
        'objs': paginator.get_page(currentpage),
    }
    return render(request, os.path.join(TEMPLATE_PATH, 'giangdays.html'), params)
    
def giangday(request, id=0):
    if not id is None and int(id) > 0:
        obj = GiangDay.objects.select_related('giaovien').get(pk=id)
    else:
        obj = GiangDay()
    if request.method == 'POST':
        data = request.POST.copy()
        form = GiangDayForm(data=data, instance=obj)
        if form.is_valid():
            try:
                giaovien = GiaoVien.objects.get(id=data.get('giaovien'))
                obj.giaovien = giaovien
                obj.save()
            except ObjectDoesNotExist:
                return HttpResponseRedirect(reverse('studentmanagement:giangdays'))
            return HttpResponseRedirect(reverse('studentmanagement:giangdays'))
    else:
        form = GiangDayForm(instance=obj)
    
    monhoc = obj.mon
    giaoviens = GiaoVien.objects.filter(monhocs=monhoc)
    print(giaoviens.values())
        
    params = {
        'form': form,
        'giaoviens': giaoviens,
        'obj': obj,
    }
    return render(request, os.path.join(TEMPLATE_PATH, 'giangday.html'), params)
    
