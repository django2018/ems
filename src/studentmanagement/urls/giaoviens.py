from django.urls import path
from .. import views

urlpatterns = [
    path('giaovien/', views.cu_giaovien, name='giaovien'),
    path('giaovien/<int:id>/', views.cu_giaovien, name='giaovien'),
    path('delete/giaovien/<int:id>/', views.delete_giaovien, name='delete_giaovien'),
    path('giaoviens/', views.show_giaoviens, name='giaoviens'),
    
    path('giangdays/', views.giangdays, name='giangdays'),
    path('giangday/<int:id>', views.giangday, name='giangday'),
    
]