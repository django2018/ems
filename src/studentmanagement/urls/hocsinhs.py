from django.urls import path
from .. import views

urlpatterns = [
    path('lop/', views.cu_lop, name='lop'),
    path('lop/<int:id>/', views.cu_lop, name='lop'),
    path('delete/lop/<int:id>/', views.delete_lop, name='delete_lop'),
    path('lops/', views.show_lops, name='lops'),
    
    path('hocsinh/', views.cu_hocsinh, name='hocsinh'),
    path('hocsinh/<int:id>/', views.cu_hocsinh, name='hocsinh'),
    path('delete/hocsinh/<int:id>/', views.delete_hocsinh, name='delete_hocsinh'),
    path('hocsinhs/', views.show_hocsinhs, name='hocsinhs'),
    
    path('monhoc/', views.cu_monhoc, name='monhoc'),
    path('monhoc/<int:id>/', views.cu_monhoc, name='monhoc'),
    path('delete/monhoc/<int:id>/', views.delete_monhoc, name='delete_monhoc'),
    path('monhocs/', views.show_monhocs, name='monhocs'),
    
    path('danhsachlop/<int:malop>', views.show_danhsachlop, name='danhsachlop'),
    path('bangdiem/<int:mahocsinh>', views.show_bangdiem, name='bangdiem'),
    
]