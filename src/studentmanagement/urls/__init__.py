from django.urls import path
from . import giaoviens
from . import hocsinhs

app_name = 'studentmanagement'

urlpatterns = []
urlpatterns += giaoviens.urlpatterns
urlpatterns += hocsinhs.urlpatterns