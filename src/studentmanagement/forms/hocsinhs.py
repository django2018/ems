from django import forms
from ..models import *

class LopForm(forms.ModelForm):
    class Meta:
        model = Lop
        fields = '__all__'
    
    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            
class HocSinhForm(forms.ModelForm):
    class Meta:
        model = HocSinh
        fields = '__all__'
    
    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            
class MonHocForm(forms.ModelForm):
    class Meta:
        model = MonHoc
        fields = '__all__'
    
    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            
