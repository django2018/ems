from django import forms
from ..models import *

class GiaoVienForm(forms.ModelForm):
    class Meta:
        model = GiaoVien
        fields = '__all__'
    
    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            
class GiangDayForm(forms.ModelForm):
    class Meta:
        model = GiangDay
        fields = ('giaovien', )
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)