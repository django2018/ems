from django import template
from django.conf import settings

register = template.Library()

@register.inclusion_tag('templatetags/giaoviens/giangday.html')
def giangday(object):
    from ..models import GiaoVien
    objects = GiaoVien.objects.all()
    return {
        'objects': objects,
        'object': object,
    }