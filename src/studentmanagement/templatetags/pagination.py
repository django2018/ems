from django import template
from django.conf import settings

register = template.Library()

@register.inclusion_tag('templatetags/pagination.html')
def pagination(objects):
    '''In settings.py add 
    PAGINATE_BY = 2 # number of objects in a page
    TOTAL_PAGES = 5 # number pages of paginator to user choose
    '''
    if objects:
        num_pages = objects.paginator.num_pages
        currentpage = objects.number
        TOTAL_PAGES = settings.TOTAL_PAGES
        STEP = int((TOTAL_PAGES + 1)/2)
        
        if num_pages <= TOTAL_PAGES or currentpage <= STEP:
            pageranges = range(1, min(num_pages + 1, TOTAL_PAGES + 1))
        elif currentpage > num_pages - STEP:
            pageranges = range(num_pages - TOTAL_PAGES + 1,num_pages + 1)
        else:
            pageranges = range(currentpage - STEP + 1, currentpage + STEP)
        return {
            'objects': objects,
            'pageranges': pageranges,
        }