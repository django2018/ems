from django.db import models
from django.urls import reverse
from .hocsinh import *

class Nguoi(models.Model):
    maso = models.CharField(default=None, max_length=6, blank=False, null=False, unique=True, verbose_name='Mã số')
    ten = models.CharField(max_length=100, verbose_name='Tên')
    ngaysinh = models.DateField(verbose_name='Ngày sinh')
    diachi = models.CharField(max_length=100, verbose_name='Địa chỉ')

    class Meta:
        abstract = True
        app_label = 'studentmanagement'
        ordering = ['id']
        
    def __str__(self):
        return '{}'.format(self.ten)
        
    def get_absolute_url(self):
        return reverse('studentmanagement:giaovien', kwargs={'id': self.id})
        
    def delete_absolute_url(self):
        return reverse('studentmanagement:delete_giaovien', kwargs={'id': self.id})
        
class GiaoVien(Nguoi):
    monhocs = models.ManyToManyField('MonHoc', verbose_name='Môn học')
    dangday = models.BooleanField(null=False, blank=False, default=True, verbose_name='Đang dạy')
    

class GiangDay(models.Model):
    lop = models.ForeignKey('Lop', on_delete=models.CASCADE, verbose_name='Lớp')
    mon = models.ForeignKey('MonHoc', on_delete=models.CASCADE, verbose_name='Môn')
    giaovien = models.ForeignKey('GiaoVien', null=True, blank=True, on_delete=models.CASCADE, verbose_name='Giáo viên')

    class Meta:
        app_label = 'studentmanagement'
    
