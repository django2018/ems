from django.db import models
from django.urls import reverse

class Lop(models.Model):
    malop = models.IntegerField(verbose_name='Mã lớp')
    tenlop = models.CharField(max_length=30, blank=False, null=False, verbose_name='Tên lớp')
    diengiai = models.CharField(max_length=30, verbose_name='Diễn giải')
    lopchuyen = models.BooleanField(verbose_name='Lớp chuyên')
    tengiaovienchunhiem = models.CharField(max_length=30, verbose_name='Giáo viên chủ nhiệm')
    
    class Meta:
        ordering = ['id']
        app_label = 'studentmanagement'
    
    def __str__(self):
        return '{}'.format(self.tenlop)
    
    def get_absolute_url(self):
        return reverse('studentmanagement:lop', kwargs={'id': self.id})
    
    def delete_absolute_url(self):
        return reverse('studentmanagement:delete_lop', kwargs={'id': self.id})

class HocSinh(models.Model):
    mahocsinh = models.IntegerField(verbose_name='Mã học sinh')
    tenhocsinh = models.CharField(max_length=30, blank=False, null=False, verbose_name='Tên học sinh')
    ngaysinh = models.DateField(verbose_name='Ngày sinh')
    lop = models.ForeignKey("Lop", on_delete=models.CASCADE, verbose_name='Lớp')
    
    class Meta:
        ordering = ['id']
        app_label = 'studentmanagement'
    
    def __str__(self):
        return '{}'.format(self.tenhocsinh)
    
    def get_absolute_url(self):
        return reverse('studentmanagement:hocsinh', kwargs={'id': self.id})
    
    def delete_absolute_url(self):
        return reverse('studentmanagement:delete_hocsinh', kwargs={'id': self.id})
    
class MonHoc(models.Model):
    mamon = models.CharField(max_length=10, blank=False, null=False, unique=True, verbose_name='Mã Môn')
    tenmon = models.CharField(max_length=10, blank=False, null=False, verbose_name='Tên Môn')
    
    class Meta:
        ordering = ['id']
        app_label = 'studentmanagement'
    
    def __str__(self):
        return '{}'.format(self.tenmon)
    
    def get_absolute_url(self):
        return reverse('studentmanagement:monhoc', kwargs={'id': self.id})
    
    def delete_absolute_url(self):
        return reverse('studentmanagement:delete_monhoc', kwargs={'id': self.id})
    
class BangDiem(models.Model):
    hocsinh = models.ForeignKey('HocSinh', on_delete=models.CASCADE, verbose_name='Học Sinh')
    monhoc = models.ForeignKey('MonHoc', on_delete=models.CASCADE, verbose_name='Môn Học')
    mieng1 = models.IntegerField(blank=False, null=False, default=0, verbose_name='Miệng')
    mieng2 = models.IntegerField(blank=False, null=False, default=0, verbose_name='Mười Lăm Phút')
    tiet = models.IntegerField(blank=False, null=False, default=0, verbose_name='Một Tiết')
    hocky = models.IntegerField(blank=False, null=False, default=0, verbose_name='Học Kỳ')
     
    class Meta:
        ordering = ['id']
        app_label = 'studentmanagement'
     
    def __str__(self):
        return '{}'
    
#     def get_absolute_url(self):
#         return reverse('studentmanagement:mon', kwargs={'id': self.id})
#     
#     def delete_absolute_url(self):
#         return reverse('studentmanagement:delete_mon', kwargs={'id': self.id})
        
        
    