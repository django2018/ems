#!/bin/bash
export PYTHONPATH="/home/ductam/virtualenvs/ems/bin/activate"
source $PYTHONPATH
# ---get the python3.6 path---
# which python3.6
# ---get the django version ---
# django-admin.py --version
echo "1. runserver in localhost"
echo "2. databases"
read -p "Enter your choice: " choice
if [ $choice -eq 1 ]; then
	python3.6 manage.py runserver
elif [ $choice -eq 2 ]; then
	python3.6 manage.py makemigrations
	python3.6 manage.py migrate
else
	bash
fi
./runserver.sh
