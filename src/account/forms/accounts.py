from django.forms import ModelForm
from django import forms
from django.contrib.auth.models import User
from django.utils.translation import gettext as _
import re

class RegistryForm(ModelForm):
    passwordagain = forms.CharField(max_length=128, label='Nhập lại mật khẩu')
    class Meta:
        model = User
        fields = {'username', 'password', 'passwordagain', 'first_name', 'last_name', 'email'}
    
    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.fields['username'].label = 'Tài khoản'
            self.fields['password'].label = 'Mật khẩu'
            self.fields['first_name'].label = 'Tên'
            self.fields['last_name'].label = 'Họ'
            
            self.fields['password'].widget = forms.PasswordInput()
            self.fields['passwordagain'].widget = forms.PasswordInput()
            
class LoginForm(forms.Form):
    username = forms.CharField(max_length=128, label='Tên đăng nhập')
    password = forms.CharField(widget=forms.PasswordInput(), max_length=150)
    
    def clean_username(self):
        data = self.cleaned_data['username']
        regex = r'^[\w.@+-]+\Z'
        regex_matches = re.search(regex, data)
        if not regex_matches:
            raise forms.ValidationError(
                _('Nhập đúng tài khoản. '
                   'Giá trị này chứa các kí tự chữ, '
                   'số, và kí tự @/./+/-/_.'
                ),
                code='invalid',
            )
        return data
    
    def clean(self):
        super().clean()
        username = self.cleaned_data['username']
        password = self.cleaned_data['password']
        
        try:
            obj = User.objects.get(username=username, password=password)
        except:
            raise forms.ValidationError(
                _('Tài khoản hoặc mật khẩu không đúng'),
                code='invalid',
            )
            
         
        return self.cleaned_data
    
#     def is_valid(self):
#         super().is_valid()
#         username = self.cleaned_data.get('username')
#         try:
#             obj = User.objects.get(username=username)
#         except:
#             obj = None
#         
#         if obj is None:
#             return False
#         
#         return self.is_valid()

