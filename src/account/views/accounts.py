import os
from django.shortcuts import render
from django.urls.base import reverse
from django.http import HttpResponseRedirect
from ..forms import *
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import authenticate, login, logout

TEMPLATE_PATH = "account"

def registry(request):
    if request.method == 'POST':
        data = request.POST.copy()
        registryform = RegistryForm(data=data)
        if registryform.is_valid():
            User.objects.create_user(
                username=data.get('username'),
                password=data.get('password'),
                email=data.get('email'),
            )
#             registry = registryform.save()
            return HttpResponseRedirect(reverse('core:homepage'))
    else:
        registryform = RegistryForm()
        
    params = {
        'form': registryform,
    }
    
    return render(request, os.path.join(TEMPLATE_PATH, 'registry.html'), params)

def login1(request):
    if request.method == 'POST':
        data = request.POST.copy()
        filters = {
            'username': data.get('username'), 
            'password': data.get('password'),
        }
        user = authenticate(request, **filters)
        if user is not None:
            login(request, user)
            return HttpResponseRedirect(reverse('core:homepage'))
        else:
            loginform = LoginForm(data=data)
            error = 'Invalid user'
            
#         try:
#             filters = {
#                 'username': data.get('username'), 
#                 'password': data.get('password'),
#             }
#             user = User.objects.get(**filters)
#             request.session['username'] = user.username
#             return HttpResponseRedirect(reverse('core:homepage'))
#         except ObjectDoesNotExist:
#             loginform = LoginForm(data=data)
#             error = 'Invalid user'
    else:
        loginform = LoginForm()
        
    params = {
        'form': loginform,
        'error': error if 'error' in locals() else '',
    }
    
    return render(request, os.path.join(TEMPLATE_PATH, 'login.html'), params)

def logout1(request):
    logout(request)
    return HttpResponseRedirect(reverse('core:homepage'))
        
        