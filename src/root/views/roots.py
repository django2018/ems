import os
from django.shortcuts import render
from django.urls.base import reverse
from django.http import HttpResponseRedirect
from ..forms import *
from django.core.exceptions import ObjectDoesNotExist

TEMPLATE_PATH = "roots"

def dashboard(request):
    if request.user.is_authenticated and request.user.is_superuser:
        params = {
            
        }
        return render(request, os.path.join(TEMPLATE_PATH, 'dashboard.html'), params)
    else:
        return HttpResponseRedirect(reverse('core:homepage'))