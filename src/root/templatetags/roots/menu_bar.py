from django import template

register = template.Library()

@register.inclusion_tag('templatetags/roots/menu_bar.html', takes_context=True)
def menu(context):
    params = {
        'user': context['request'].user,
    }
    return params