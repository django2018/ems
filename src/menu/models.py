from django.db import models
from django.urls import reverse

class Menu(models.Model):
    name = models.CharField(max_length=30, blank=False, null=False, verbose_name='Tên')
    desc = models.CharField(max_length=256, blank=True, null=True, verbose_name='Mô tả')
    path = models.URLField(max_length=200, blank=True, null=True, verbose_name='Đường dẫn')
    index = models.IntegerField(default=1, blank=False, null=False, verbose_name='Thứ tự')
    parent = models.ForeignKey('self', related_name='menu_parent', blank=True, null=True, 
       on_delete=models.CASCADE, verbose_name='Cha')
    depth = models.IntegerField(default=0, blank=False, null=False, verbose_name='Độ sâu')
    isuse = models.BooleanField(blank=False, null=False, default=False, verbose_name='Được sử dụng')
    
    def __str__(self):
        return '{}'.format(self.name)
    
    def get_absolute_url(self):
        return reverse('menu:cu_menu', kwargs={'id': self.id})