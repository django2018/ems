import os

from django.shortcuts import render, redirect, get_object_or_404
from django.db.models import F
from .models import * 
from .forms import *
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings

app_name = 'menu'

TEMPLATE_PATH = "menu"

try:
    MAX_DEPTH = settings.MAX_DEPTH
except:
    MAX_DEPTH = 1

'''
def cu_menu(request, id=0):
    if id is not None and int(id) > 0:
        obj = get_object_or_404(Menu, pk=id)
    else:
        obj = Menu()
        
    if request.method == 'POST':
        data = request.POST.copy()
        files = request.FILES.copy()
        form = MenuForm(data=data, files=files, instance=obj)
        
        if form.is_valid():
            obj = form.save()
            
            return redirect('menu:cu_menu', id=obj.pk)
    else:
        form = MenuForm(instance=obj)
    
    params = {
        'form': form,
    }
    return render(request, os.path.join(TEMPLATE_PATH, 'cu_menu.html'), params)
'''

def show_menus(request):
    objs = Menu.objects.all()
    
    params = {
        'objs': objs,
        'max_depth': MAX_DEPTH,
    }
    return render(request, os.path.join(TEMPLATE_PATH, 'show_menus.html'), params)
def u_menu(request, id):
    if id is not None and int(id) > 0:
        try:
            obj = Menu.objects.get(pk=id)
        except ObjectDoesNotExist:
            return redirect('menu:show_menus')
    else:
        return redirect('menu:show_menus')
    
    if request.method == 'POST':
        data = request.POST.copy()
        files = request.FILES.copy()
        form = MenuForm(data=data, files=files, instance=obj)
        
        if form.is_valid():
            obj = form.save()
            
            return redirect('menu:show_menus')
    else:
        form = MenuForm(instance=obj)
    
    params = {
        'form': form,
    }
    return render(request, os.path.join(TEMPLATE_PATH, 'cu_menu.html'), params)

def c_menu(request):
    obj = Menu()
    
    if request.method == 'POST':
        data = request.POST.copy()
        files = request.FILES.copy()
        form = MenuForm(data=data, files=files, instance=obj)
        
        if form.is_valid():
            obj = form.save()
            Menu.objects.filter(**{
                'parent__isnull': True,
            }).exclude(**{
                'pk': obj.pk,
            }).update(index=F('index') + 1)
            
            
            return redirect('menu:show_menus')
    else:
        form = MenuForm(instance=obj)
    
    params = {
        'form': form,
    }
    return render(request, os.path.join(TEMPLATE_PATH, 'cu_menu.html'), params)

def cu_next_menu(request, id=0):
    if id is not None and int(id) > 0:
        try:
            old_obj = Menu.objects.get(pk=id)
        except ObjectDoesNotExist:
            return redirect('menu:show_menus')
    
    obj = Menu()
    
    if request.method == 'POST':
        data = request.POST.copy()
        files = request.FILES.copy()
        form = MenuForm(data=data, files=files, instance=obj)
        
        if form.is_valid():
            new_obj = form.save()
            new_obj.index = old_obj.index + 1
            new_obj.parent = old_obj.parent
            new_obj.depth = old_obj.depth
            new_obj.save()
            
            Menu.objects.filter(
                **{'parent': old_obj.parent, 'index__gt': old_obj.index}
            ).exclude(
                **{'pk': new_obj.pk,}
            ).update(index=F('index') + 1)
            
            return redirect('menu:show_menus')
    else:
        form = MenuForm(instance=obj)
    
    params = {
        'form': form,
    }
    return render(request, os.path.join(TEMPLATE_PATH, 'cu_menu.html'), params)

def cu_child_menu(request, id=0):
    if id is not None and int(id) > 0:
        try:
            old_obj = Menu.objects.get(pk=id)
            if not old_obj.depth < MAX_DEPTH:
                return redirect('menu:show_menus')
        except ObjectDoesNotExist:
            return redirect('menu:show_menus')
    
    obj = Menu()
    
    if request.method == 'POST':
        data = request.POST.copy()
        files = request.FILES.copy()
        form = MenuForm(data=data, files=files, instance=obj)
        
        if form.is_valid():
            new_obj = form.save()
            new_obj.parent = old_obj
            new_obj.depth = old_obj.depth + 1
            new_obj.save()
            
            Menu.objects.filter(
                **{'parent': old_obj,}
            ).exclude(
                **{'pk': new_obj.pk,}
            ).update(index=F('index') + 1)
            
            return redirect('menu:show_menus')
    else:
        form = MenuForm(instance=obj)
    
    params = {
        'form': form,
        'max_depth': MAX_DEPTH,
    }
    return render(request, os.path.join(TEMPLATE_PATH, 'cu_menu.html'), params)
