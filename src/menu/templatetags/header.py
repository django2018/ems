from django import template

register = template.Library()

"""
@register.inclusion_tag('templatetags/menu.html')
def menu(obj, isnext=False):
    '''
    Args:
        isnext: check the current obj is next of other objs
    '''
    params = {
        'obj': obj,
        'isnext': isnext,
    }
    return params
"""

@register.inclusion_tag('templatetags/header.html')
def header():
    pass