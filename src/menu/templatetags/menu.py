from django import template

register = template.Library()

"""
@register.inclusion_tag('templatetags/menu.html')
def menu(obj, isnext=False):
    '''
    Args:
        isnext: check the current obj is next of other objs
    '''
    params = {
        'obj': obj,
        'isnext': isnext,
    }
    return params
"""

@register.inclusion_tag('templatetags/show_menus.html', takes_context=True)
def show_menus(context, objs, parent=None):
    new_objs = objs.filter(parent=parent).order_by('index')
    params = {
        'objs': objs,
        'new_objs': new_objs,
        'parent': parent,
        'max_depth': context['max_depth'],
    }
    return params