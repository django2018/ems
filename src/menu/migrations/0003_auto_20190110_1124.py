# Generated by Django 2.0.5 on 2019-01-10 04:24

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('menu', '0002_auto_20190108_2024'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='menu',
            name='child',
        ),
        migrations.AddField(
            model_name='menu',
            name='index',
            field=models.IntegerField(default=1, verbose_name='Thứ tự'),
        ),
        migrations.AddField(
            model_name='menu',
            name='parent',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='menu_ke', to='menu.Menu', verbose_name='Cha'),
        ),
    ]
