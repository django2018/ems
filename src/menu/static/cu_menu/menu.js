$(document).ready(function() {
	$("#menu ul > li:has('ul') > a").append("<span> +</span>");
	
	$("#menu ul > li > ul").addClass("js-hideElement");
	
	$("#menu ul > li").on("mouseover", function() {
		$(this).children("ul").removeClass("js-hideElement");
		$(this).children("ul").addClass("js-showElement");
	});
	
	$("#menu ul > li").on("mouseleave", function() {
		$(this).children("ul").removeClass("js-showElement");
		$(this).children("ul").addClass("js-hideElement");
	});
});