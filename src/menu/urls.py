from django.urls import pathfrom . import viewsapp_name = 'menu'

urlpatterns = [#     path('', views.menu, name='menu'),    path('show_menus/', views.show_menus, name='show_menus'),    path('u_menu/<int:id>', views.u_menu, name='u_menu'),    path('c_menu/', views.c_menu, name='c_menu'),    path('cu_next_menu_<int:id>', views.cu_next_menu, name='cu_next_menu'),    path('cu_child_menu_<int:id>', views.cu_child_menu, name='cu_child_menu'),
]

from django.conf import settings

if settings.DEBUG:
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns
    urlpatterns += staticfiles_urlpatterns()