from django import forms
from .models import *

'''
class MenuForm(forms.ModelForm):
    class Meta:
        model = Menu
        fields = ('name', 'desc', 'path', 'root')
        
    
    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.fields['root'].required = False
'''

class MenuForm(forms.ModelForm):
    class Meta:
        model = Menu
        fields = ('name', 'desc', 'path', 'isuse')
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)