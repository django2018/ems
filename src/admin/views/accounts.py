import os
from django.shortcuts import render
from django.urls.base import reverse
from django.http import HttpResponseRedirect
from ..forms import *
from django.core.exceptions import ObjectDoesNotExist

TEMPLATE_PATH = "admin"

# def registry(request):
#     if request.method == 'POST':
#         data = request.POST.copy()
#         registryform = RegistryForm(data=data)
#         if registryform.is_valid():
#             registry = registryform.save()
#             return HttpResponseRedirect(reverse('studentmanagement:homepage'))
#     else:
#         registryform = RegistryForm()
#         
#     params = {
#         'form': registryform,
#     }
#     
#     return render(request, os.path.join(TEMPLATE_PATH, 'registry.html'), params)      