def get_user(request):
    params = {
        'user': request.session.get('user', None)
    }
    return params