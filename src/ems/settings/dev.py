from .common import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'HOST': '127.0.0.1',
        'NAME': 'ems',
        'USER': 'root',
        'PASSWORD': '',
        'PORT': '3306',
        'TEST': {
            'NAME': 'test_ems',
            'CHARSET': 'utf8',
            'COLLATION': 'utf8_general_ci',
        },
    }
}
