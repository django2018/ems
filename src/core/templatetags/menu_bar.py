from django import template

register = template.Library()

@register.inclusion_tag('templatetags/core/menu_bar.html', takes_context=True)
def menu(context):
    params = {
        'user': context['request'].user,
    }
    return params