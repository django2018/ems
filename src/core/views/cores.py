import os
from django.shortcuts import render
from django.urls.base import reverse
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User

TEMPLATE_PATH = ''

def index(request):
    params = {
        
    }
    return render(request, os.path.join(TEMPLATE_PATH, 'index.html'), params)

def dashboard(request):
    username = request.username
    user = User.objects.get(username=username)
    if user.is_superuser:
        htmlpage = 'dashboard/superuser.html'
    else:
        htmlpage = 'dashboard/giaovien.html'
    params = {
        
    }
    return render(request, os.path.join(TEMPLATE_PATH, htmlpage), params)
